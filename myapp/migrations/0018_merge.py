from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0016_jobapplication_joboffers_applicants'),
        ('myapp', '0017_enable_pg_vector'),
    ]

    operations = [
    ]
