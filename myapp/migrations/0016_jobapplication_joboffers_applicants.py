# Generated by Django 5.0.6 on 2024-05-30 11:56

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0015_alter_joboffers_requirements'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobApplication',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('application_date', models.DateTimeField(auto_now_add=True)),
                ('job_offer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.joboffers')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'job_application_table',
                'unique_together': {('user', 'job_offer')},
            },
        ),
        migrations.AddField(
            model_name='joboffers',
            name='applicants',
            field=models.ManyToManyField(related_name='applied_jobs', through='myapp.JobApplication', to=settings.AUTH_USER_MODEL),
        ),
    ]
